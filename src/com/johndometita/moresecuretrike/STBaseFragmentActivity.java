package com.johndometita.moresecuretrike;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.johndometita.moresecuretrike.managers.STLocationManager;
import com.johndometita.moresecuretrike.util.SystemUiHider;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;

import android.support.v4.app.FragmentActivity;
import android.util.Log;


public class STBaseFragmentActivity extends FragmentActivity {
	
	private String TAG = "STBaseFragmentActivity";
	
	public boolean canStartApp = false;
	

	public STBaseFragmentActivity() {
		// TODO Auto-generated constructor stub
	}
	
	public boolean isGPSTurnedOn () {
		return STLocationManager.getInstance().checkIfGPSIsEnabled(this);
	}
	
	public boolean isNetworkAvailable() 
	{
		boolean result = true;
		ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    result = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
	    
	    if(!result) {
	    	AlertDialog.Builder ab = new AlertDialog.Builder(STBaseFragmentActivity.this);
	    	ab.setCancelable(false);
			ab.setPositiveButton("Turn on Internet Settings", new OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent n = new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
					if(n != null)
					 startActivity(n);
					finish();
				}
			});
			
			ab.setNegativeButton("Terminate app", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
				
			});
			
			ab.setTitle("Error");
			ab.setMessage("You need to have an Internet Connection");
			ab.show();
	    }
	    return result;
	}
	
	

	protected void onResume() {
		super.onResume();
		
		String message = "";
		int errorCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		switch(errorCode) {
			case ConnectionResult.SUCCESS:
				message = "Google Services ok";
				if(isGPSTurnedOn())
					if(isNetworkAvailable())
						canStartApp = true;
				
				
				return;
			case ConnectionResult.SERVICE_MISSING:
				message = "Google Services are missing. Please go to Google Play and download the latest Google services.";
				break;
			case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
				message = "Google Services installed on your device needs update. Please update before you use this application";
				break;
			case ConnectionResult.SERVICE_DISABLED:
				message = "Google Services installed on your device is disabled. Go to Settings > Apps Manager > Then enable Google Services.";
				break;
			case ConnectionResult.SERVICE_INVALID:
				message = "Google Services is invalid. Try to install the one coming from the Google Play.";
				break;
			case ConnectionResult.DATE_INVALID:
				message = "Google Services is installed. But your device seems to have an invalid date. Please fix this on your Settings > Date and Time";
				break;
		}
		
		GooglePlayServicesUtil.getErrorDialog(errorCode, this, 101, new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				AlertDialog.Builder ab = new AlertDialog.Builder(STBaseFragmentActivity.this);
				ab.setPositiveButton("Close app", new OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
				
				ab.setTitle("Error");
				ab.setMessage("You need to have Google Services before you can use this application.");
				ab.show();
			}
		}).show();
		
		Log.d(TAG, message);
	}
	
}

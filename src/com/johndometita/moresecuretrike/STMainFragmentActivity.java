package com.johndometita.moresecuretrike;

import org.json.JSONArray;

import com.crittercism.app.Crittercism;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.johndometita.moresecuretrike.fragment.STCalculatorFragment;
import com.johndometita.moresecuretrike.fragment.STCalculatorFragment.CalculatorCallBack;
import com.johndometita.moresecuretrike.fragment.STLoginFragment;
import com.johndometita.moresecuretrike.fragment.STLoginFragment.STLoginCallBack;
import com.johndometita.moresecuretrike.managers.STDataManager;
import com.johndometita.moresecuretrike.managers.STDataManager.STDataManagerCallBack;
import com.johndometita.moresecuretrike.managers.STDataManager.STDataManagerLandmarkCallBack;
import com.johndometita.moresecuretrike.managers.STDataManager.STDataManagerPostDataCallBack;
import com.johndometita.moresecuretrike.model.STLocation;
import com.johndometita.moresecuretrike.model.STLocation.TripStatus;
import com.johndometita.moresecuretrike.model.STTrip;
import com.johndometita.moresecuretrike.model.STUser;
import com.johndometita.moresecuretrike.util.SystemUiHider;
import com.johndometita.moresecuretrike.widget.ProgressHUD;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class STMainFragmentActivity extends STBaseFragmentActivity implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,
LocationListener	{
	
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = false;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;
	
	private TextView mStatusView;
	private Button mStartBtn;
	private Button mGasBtn;
	private Button mRestBtn;
	private View mPassengerControl;
	private View mLuggageControl;
	private STCalculatorFragment mCalculator;
	private STCalculatorFragment mLiterCalculator;
	private ProgressHUD mProgressHUD;  
	private static String TAG = "STMainFragmentActivity";
	
	/*
	 * To be used for getting locations
	 */
	LocationClient mLocationClient;

	boolean mUpdatesRequested;
	
	/*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	
	// Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 2;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
    
    
    LocationRequest mLocationRequest;
    
	
	public STMainFragmentActivity() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Crittercism.initialize(getApplicationContext(), "531501608633a406c2000001");
		
		setContentView(R.layout.fragmentactivity_main);
		STDataManager.startInstance(getApplicationContext());

		final View controlsView = findViewById(R.id.fullscreen_content_controls);
		
		mGasBtn = (Button)controlsView.findViewById(R.id.btn_gas);
		mRestBtn = (Button)controlsView.findViewById(R.id.btn_rest);
		
		
		
		mGasBtn.setOnClickListener(new OnClickListener () {
			@Override
			public void onClick(View v) {
				showAlertDialog(mGasBtn);
			}
		});
		
		mRestBtn.setOnClickListener(new OnClickListener () {
			@Override
			public void onClick(View v) {
				if(STDataManager.getInstance().getCurrentStatus() == TripStatus.ON_TRIP) {
					Toast.makeText(getApplication(), "Hindi pa maari sapagkat ikaw ay nasa byahe. Tapusin muna ang byahe.", Toast.LENGTH_SHORT).show();
				}else {
					showAlertDialog(mRestBtn);
				}
			}
		});
		
		final View contentView = findViewById(R.id.fullscreen_content);
		
		mStatusView = (TextView)findViewById(R.id.textView_status);
	
		mStartBtn = (Button) findViewById(R.id.btn_start);
		mStartBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final TripStatus status = STDataManager.getInstance().getCurrentStatus();
				final TripStatus futureStatus = STDataManager.getInstance().futureStatus;
				
				if(status == TripStatus.ON_TRIP && futureStatus == TripStatus.END_TRIP && STDataManager.getInstance().getTempLuggageCount() > 0) {
					Toast.makeText(getApplication(), "You can't end a trip with a luggage. Unload all your luggage first then try again.", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(status == TripStatus.IDLE || status == TripStatus.ON_TRIP) {
					showCalculator(futureStatus, "Pamasahe", new CalculatorCallBack() {
						public void didCancel() {
							hideCalculator();
						}
						public void didFinish(float amount) {
							final int numberOfPassengers = STDataManager.getInstance().getTempPassengerCount();
							final int numberOfLuggage = STDataManager.getInstance().getTempLuggageCount();
							final float fare = amount;
							if(status == TripStatus.IDLE) {
								Toast.makeText(getApplication(), "About to start a trip  " + fare + " ,"  + numberOfPassengers + " ," + numberOfLuggage, Toast.LENGTH_SHORT).show();
								mProgressHUD = ProgressHUD.show(STMainFragmentActivity.this,"Recording data...", true,false,null);
								STDataManager.getInstance().sendRecentDataToServer(new STDataManagerPostDataCallBack () {

									@Override
									public void onResult(boolean result) {
										Log.d(TAG, "START_TRIP");
										
										if(!result) {
											mProgressHUD.dismiss();
											Toast.makeText(STMainFragmentActivity.this, "Failed to send data to server...please try again. Make sure you have a stable internet connection.", Toast.LENGTH_SHORT).show();
											return;
										}
										STDataManager.getInstance().createNewTrip(numberOfPassengers, numberOfLuggage, fare);
										STMainFragmentActivity.this.updateStatus();
										hideCalculator();
										mProgressHUD.dismiss();
									}
									
								});
								
							}else if(status == TripStatus.ON_TRIP) {
								if(futureStatus == TripStatus.END_TRIP) {
									mProgressHUD = ProgressHUD.show(STMainFragmentActivity.this,"Recording data...", true,false,null);
									STDataManager.getInstance().sendRecentDataToServer(new STDataManagerPostDataCallBack () {
										@Override
										public void onResult(boolean result) {
											Log.d(TAG, "END_TRIP");
											if(!result) {
												mProgressHUD.dismiss();
												Toast.makeText(STMainFragmentActivity.this, "Failed to send data to server...please try again. Make sure you have a stable internet connection.", Toast.LENGTH_SHORT).show();
												return;
											}
											//go idle again
											STDataManager.getInstance().endTrip(numberOfPassengers, numberOfLuggage, fare);
											updateButton(TripStatus.IDLE);
											STMainFragmentActivity.this.updateStatus();
											hideCalculator();
											mProgressHUD.dismiss();
										}
										
									});
								}else if(futureStatus == TripStatus.ON_TRIP_ADD || futureStatus == TripStatus.ON_TRIP_DROP) {
																
									mProgressHUD = ProgressHUD.show(STMainFragmentActivity.this,"Recording data...", true,false,null);
									STDataManager.getInstance().sendRecentDataToServer(new STDataManagerPostDataCallBack () {

										@Override
										public void onResult(boolean result) {
											if(!result) {
												mProgressHUD.dismiss();
												Toast.makeText(STMainFragmentActivity.this, "Failed to send data to server...please try again. Make sure you have a stable internet connection.", Toast.LENGTH_SHORT).show();
												return;
											}
											STDataManager.getInstance().updateTrip(numberOfPassengers, numberOfLuggage, fare, futureStatus);
											Log.d(TAG, STLocation.getStatusAsString(futureStatus));
											STMainFragmentActivity.this.updateStatus();
											hideCalculator();
											mProgressHUD.dismiss();
										}
										
									});
								}
							}
							
						}
					});
				}else if (status == TripStatus.REFUEL) {
					
					
				}else if (status == TripStatus.ON_BREAK) {
					showAlertDialog(mStartBtn);
				}
			}
			
		});
		
		mPassengerControl = (View)findViewById(R.id.widget_passenger);
		mLuggageControl = (View)findViewById(R.id.widget_luggage);
		
		setupLuggageWidget();
		setupPassengerWidget();
		
		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		mSystemUiHider = SystemUiHider.getInstance(this, contentView,
				HIDER_FLAGS);
		mSystemUiHider.setup();
		mSystemUiHider
				.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
					// Cached values.
					int mControlsHeight;
					int mShortAnimTime;

					@Override
					@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
					public void onVisibilityChange(boolean visible) {
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
							// If the ViewPropertyAnimator API is available
							// (Honeycomb MR2 and later), use it to animate the
							// in-layout UI controls at the bottom of the
							// screen.
							if (mControlsHeight == 0) {
								mControlsHeight = controlsView.getHeight();
							}
							if (mShortAnimTime == 0) {
								mShortAnimTime = getResources().getInteger(
										android.R.integer.config_shortAnimTime);
							}
							controlsView
									.animate()
									.translationY(visible ? 0 : mControlsHeight)
									.setDuration(mShortAnimTime);
						} else {
							// If the ViewPropertyAnimator APIs aren't
							// available, simply show or hide the in-layout UI
							// controls.
							controlsView.setVisibility(visible ? View.VISIBLE
									: View.GONE);
						}

						if (visible && AUTO_HIDE) {
							// Schedule a hide().
							delayedHide(AUTO_HIDE_DELAY_MILLIS);
						}
					}
				});

		// Set up the user interaction to manually show or hide the system UI.
		contentView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (TOGGLE_ON_CLICK) {
					mSystemUiHider.toggle();
				} else {
					mSystemUiHider.show();
				}
			}
		});

		
		mLocationRequest =  LocationRequest.create();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
		
		mGasBtn.setOnTouchListener(mDelayHideTouchListener);
		mRestBtn.setOnTouchListener(mDelayHideTouchListener);
		
		this.mLocationClient = new LocationClient(this, this, this);
		this.mUpdatesRequested = false;
		
	}
	
	@Override 
	protected void onStart() {
		super.onStart();
		if(!mLocationClient.isConnected())
			mLocationClient.connect();
	}
	
	@Override 
	public boolean onCreateOptionsMenu (Menu menu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_menu, menu);
	    

		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu (Menu menu) {
		 //hide others as needed
		if(menu.findItem(R.id.settings).hasSubMenu()) {
			Menu subMenu = menu.findItem(R.id.settings).getSubMenu();
			
			STUser user = STDataManager.getInstance().currentUser;
			
			if(user == null) 
				return false;
			
			if(user.hasGarage())
				subMenu.removeItem(R.id.btn_garage);
			
			if(user.hasHome())
				subMenu.removeItem(R.id.btn_home);
			
			if(user.hasTerminal1())
				subMenu.removeItem(R.id.btn_terminal_1);
			
			if(user.hasTerminal2())
				subMenu.removeItem(R.id.btn_terminal_2);
			
			if(user.hasTerminal3())
				subMenu.removeItem(R.id.btn_terminal_3);
			
			if(!subMenu.hasVisibleItems()) {
				return false;
			}
		}

		return (STDataManager.getInstance().currentUser != null);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		AlertDialog.Builder ab = new AlertDialog.Builder(this);
		ab.setMessage("Sigurado ka ba sa lokasyon na ito?");
		ab.setCancelable(false);
		ab.setNegativeButton("HINDI", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				
			}
		});
		String l = "";
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.settings:
	            return true;
	        case R.id.btn_garage:
	        	ab.setTitle("GARAHE");
	        	l = "GARAGE";
	        	break;
	        case R.id.btn_home:
	        	ab.setTitle("HOME");
	        	l = "HOME";
	        	break;
	        case R.id.btn_terminal_1:
	        	ab.setTitle("TERMINAL 1");
	        	l = "TERMINAL_1";
	        	break;
	        case R.id.btn_terminal_2:
	        	ab.setTitle("TERMINAL 2");
	        	l = "TERMINAL_2";
	        	break;
	        case R.id.btn_terminal_3:
	        	ab.setTitle("TERMINAL 3");
	        	l = "TERMINAL_3";
	        	break;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	    
	    final String landmark = l;
	    
	    ab.setPositiveButton("OO", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				STDataManager.getInstance().sendLandmarkDataToServer(landmark, new STDataManagerCallBack () {
					@Override
					public void onResult(boolean success, String message) {
						if(success) {
							Toast.makeText(getApplication(), "Your location for " + landmark + " has been recorded. Thank you!", Toast.LENGTH_SHORT).show();
							STMainFragmentActivity.this.invalidateOptionsMenu();
						}else {
							Toast.makeText(getApplication(), message, Toast.LENGTH_SHORT).show();
						}
						
					}
				});
			}
		});
	    
    	ab.show();
        return true;
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
		delayedHide(100);
	}

	
	protected void onResume() {
		super.onResume();
		
		//check if login
		if(STDataManager.getInstance().currentUser == null) {
			
			//try autologin first
			STDataManager.getInstance().autologin(new STDataManagerCallBack () {

				@Override
				public void onResult(boolean success, String message) {
					if(success) {
						mGasBtn.setVisibility(View.VISIBLE);
						mRestBtn.setVisibility(View.VISIBLE);
						STMainFragmentActivity.this.invalidateOptionsMenu();
						updateStatus();
						
						//check for the landmarks if updated
						STDataManager.getInstance().getUserLandmarks(new STDataManagerLandmarkCallBack () {
							@Override
							public void onResult(JSONArray landmarks) {
								// TODO Auto-generated method stub
								STMainFragmentActivity.this.invalidateOptionsMenu();
							}
						});
						
					}else {
						final FragmentManager fm = getSupportFragmentManager();
						if(fm.findFragmentByTag("login") != null)
							return;
						
						mGasBtn.setVisibility(View.INVISIBLE);
						mRestBtn.setVisibility(View.INVISIBLE);
						final STLoginFragment login = new STLoginFragment();
						login.setCallBack(new STLoginCallBack() {

							@Override
							public void loginSuccess(STUser user) {
								mGasBtn.setVisibility(View.VISIBLE);
								mRestBtn.setVisibility(View.VISIBLE);
								updateStatus();
								STMainFragmentActivity.this.invalidateOptionsMenu();
								Toast.makeText(getApplication(), "Welcome " + user.getUserName(), Toast.LENGTH_SHORT).show();
								fm.beginTransaction().remove(login).commit();
								
								//check for the landmarks if updated
								STDataManager.getInstance().getUserLandmarks(new STDataManagerLandmarkCallBack () {
									@Override
									public void onResult(JSONArray landmarks) {
										// TODO Auto-generated method stub
										STMainFragmentActivity.this.invalidateOptionsMenu();
									}
								});
							}

							@Override
							public void loginFailed(String errorMessage) {
								Toast.makeText(getApplication(), errorMessage, Toast.LENGTH_SHORT).show();
							}
							
						});
						fm.beginTransaction().add(R.id.fullscreen_content, (Fragment)login, "login").commit();
					}
					
					
				}
				
			});
		
			return;
		}
		
		this.updateStatus();
	}
	/**
	 * Touch listener to use for in-layout UI controls to delay hiding the
	 * system UI. This is to prevent the jarring behavior of controls going away
	 * while interacting with activity UI.
	 */
	View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (AUTO_HIDE) {
				delayedHide(AUTO_HIDE_DELAY_MILLIS);
			}
			return false;
		}
	};

	Handler mHideHandler = new Handler();
	Runnable mHideRunnable = new Runnable() {
		@Override
		public void run() {
			mSystemUiHider.hide();
		}
	};

	/**
	 * Schedules a call to hide() in [delay] milliseconds, canceling any
	 * previously scheduled calls.
	 */
	private void delayedHide(int delayMillis) {
		mHideHandler.removeCallbacks(mHideRunnable);
		mHideHandler.postDelayed(mHideRunnable, delayMillis);
	}
	
	private AlphaAnimation fadeIn = new AlphaAnimation(0.0f , 0.8f ); 
	private AlphaAnimation fadeOut = new AlphaAnimation( 0.8f , 0.0f ); 
	private void updateStatus () {
		TripStatus status = STDataManager.getInstance().getCurrentStatus();
		mStatusView.setText(STLocation.getStatusAsString(status));
		if(status == TripStatus.ON_TRIP) {
			fadeIn.setDuration(1200);
			fadeIn.setRepeatCount(Animation.INFINITE);
			fadeIn.setRepeatMode(Animation.RESTART);
			fadeIn.setFillAfter(true);
			fadeOut.setDuration(1200);
			fadeOut.setFillAfter(true);
			fadeOut.setRepeatCount(Animation.INFINITE);
			fadeOut.setRepeatMode(Animation.RESTART);
			fadeOut.setStartOffset(fadeIn.getStartOffset());
			mStatusView.startAnimation(fadeIn);
			mStatusView.startAnimation(fadeOut);
			
			//hide the mainActionButton
			this.mStartBtn.setVisibility(View.INVISIBLE);
		}else {
			if(mStatusView.getAnimation() != null)
				mStatusView.getAnimation().cancel();
		}
	}
	
	private void updateButton (TripStatus status) {
		Drawable d = null;
		Resources r = getResources();
		STDataManager.getInstance().futureStatus = status;
		
		switch(status) {
		case IDLE:
			d = r.getDrawable(R.drawable.rounded_corner__grey);
			this.mStartBtn.setEnabled(false);
			this.mStartBtn.setBackground(d);
			this.mStatusView.setText("IDLE");
			this.mStatusView.setTextColor(r.getColor(R.color.button_grey));
			this.hideActionButton();
			this.hideLuggageControl();
			return;
		case START_TRIP:
			d = r.getDrawable(R.drawable.rounded_corner__green);
			this.mStatusView.setTextColor(r.getColor(R.color.button_green));
			this.mStartBtn.setText("i-Record (New Trip)");
			break;
		case ON_TRIP_DROP:
			d = r.getDrawable(R.drawable.rounded_corner__green);
			this.mStatusView.setTextColor(r.getColor(R.color.button_green));
			this.mStartBtn.setText("i-Record (Magbababa)");
			break;
		case ON_TRIP_ADD:
			d = r.getDrawable(R.drawable.rounded_corner__green);
			this.mStatusView.setTextColor(r.getColor(R.color.button_green));
			this.mStartBtn.setText("i-Record (Magsasakay)");
			break;
		case ON_TRIP:
			d = r.getDrawable(R.drawable.rounded_corner__green);
			this.mStatusView.setTextColor(r.getColor(R.color.button_green));
			break;
		case END_TRIP:
			d = r.getDrawable(R.drawable.rounded_corner__red);
			this.mStatusView.setTextColor(r.getColor(R.color.button_red));
			this.mStartBtn.setText("END TRIP");
			break;
		case REFUEL:
			d = r.getDrawable(R.drawable.rounded_corner__yellow);
			this.mStatusView.setTextColor(r.getColor(R.color.button_yellow));
			break;
		case ON_BREAK:
			d = r.getDrawable(R.drawable.rounded_corner__red);
			this.mStatusView.setTextColor(r.getColor(R.color.button_red));
			this.mStatusView.setText("ON A BREAK");
			this.mStartBtn.setText("End BREAK");
			this.showActionButton();
			break;
		}
		
		this.mStartBtn.setEnabled(true);
		this.mStartBtn.setBackground(d);
		
	}
	
	private void setupPassengerWidget() {
		Button add = (Button)this.mPassengerControl.findViewById(R.id.btn_add);
		Button minus = (Button)this.mPassengerControl.findViewById(R.id.btn_minus);
		final TextView countLabel = (TextView)this.mPassengerControl.findViewById(R.id.textView_count);
		TextView label = (TextView)this.mPassengerControl.findViewById(R.id.textView_label);
		label.setText("PASAHERO");
		add.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TripStatus s = STDataManager.getInstance().getCurrentStatus();
				//if the status is resting, ignore
				if(s == TripStatus.ON_BREAK) {
					return;
				}
				
				int count = STDataManager.getInstance().getTempPassengerCount();
				count++;
				if(s == TripStatus.IDLE) {
					//from IDLE -> always START_TRIP
					showActionButton();
					updateButton(TripStatus.START_TRIP);
				}else if(s == TripStatus.ON_TRIP) {
					STTrip currentTrip = STDataManager.getInstance().getCurrentTrip();
					if(count > currentTrip.lastNumberOfPassengers) {
						showActionButton();
						updateButton(TripStatus.ON_TRIP_ADD);
						countLabel.setTextColor(getResources().getColor(R.color.button_green));
					}else if(count == currentTrip.lastNumberOfPassengers) {
						hideActionButton();
						countLabel.setTextColor(getResources().getColor(R.color.dark_gray));
					}else {
						updateButton(TripStatus.ON_TRIP_DROP);
						countLabel.setTextColor(getResources().getColor(R.color.button_red));
					}
				}
				showLuggageControl();
				STDataManager.getInstance().setTempPassengerCount(count);
				countLabel.setText("" + count);
			}
		});
		
		minus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int count = STDataManager.getInstance().getTempPassengerCount();
				if(count == 0) return;
				count--;
				
				//check status
				TripStatus s = STDataManager.getInstance().getCurrentStatus();
				//if on break ignore
				if(s == TripStatus.ON_BREAK) {
					return;
				}
				if(s == TripStatus.IDLE) {
					if(count == 0) {
						//hide the luggage
						//you should have at least 1 passenger to load a luggage
						hideLuggageControl();
						hideActionButton();
					}else {
						showActionButton();
						showLuggageControl();
					}
				}else if (s == TripStatus.ON_TRIP) {
					STTrip currentTrip = STDataManager.getInstance().getCurrentTrip();
					//check the current trip
					Log.d("REDUCE PASSENGER", "count " + count + " currentPassengerCount: " + currentTrip.lastNumberOfPassengers);
					if(count < currentTrip.lastNumberOfPassengers && count > 0) {
						showActionButton();
						updateButton(TripStatus.ON_TRIP_DROP);
						countLabel.setTextColor(getResources().getColor(R.color.button_red));
					}else if (count == 0) {
						showActionButton();
						updateButton(TripStatus.END_TRIP);
						countLabel.setTextColor(getResources().getColor(R.color.button_red));
					}else if (count == currentTrip.lastNumberOfPassengers) {
						hideActionButton();
						countLabel.setTextColor(getResources().getColor(R.color.dark_gray));
					}else {
						showActionButton();
						updateButton(TripStatus.ON_TRIP_ADD);
						countLabel.setTextColor(getResources().getColor(R.color.button_green));
					}
				}
				
				STDataManager.getInstance().setTempPassengerCount(count);
				countLabel.setText("" + count);
			}
		});
	}
	
	private void setupLuggageWidget() {
		Button add = (Button)this.mLuggageControl.findViewById(R.id.btn_add);
		Button minus = (Button)this.mLuggageControl.findViewById(R.id.btn_minus);
		final TextView countLabel = (TextView)this.mLuggageControl.findViewById(R.id.textView_count);
		TextView label = (TextView)this.mLuggageControl.findViewById(R.id.textView_label);
		label.setText("BAGAHE");
		add.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//check status
				TripStatus s = STDataManager.getInstance().getCurrentStatus();
				//if on break ignore
				if(s == TripStatus.ON_BREAK) {
					return;
				}
				int count = STDataManager.getInstance().getTempLuggageCount();
				count++;
				STDataManager.getInstance().setTempLuggageCount(count);
				countLabel.setText("" + count);
			}
		});
		
		minus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//check status
				TripStatus s = STDataManager.getInstance().getCurrentStatus();
				//if on break ignore
				if(s == TripStatus.ON_BREAK) {
					return;
				}
				int count = STDataManager.getInstance().getTempLuggageCount();
				if(count == 0) return;
				count--;
				STDataManager.getInstance().setTempLuggageCount(count);
				countLabel.setText("" + count);
			}
		});
	}
	
	private void showErrorMessage(String title, String message, String positive) {
		
	}
	
	private void showCalculator(TripStatus status, String title, CalculatorCallBack cb) {
		FragmentManager fm = this.getSupportFragmentManager();
		if(fm.findFragmentByTag("calculator") != null)
			return;
		
		if(mCalculator == null) {
			mCalculator = new STCalculatorFragment();
		}
		mCalculator.setCallBack(cb);
		
		String message = "";
		STTrip currentTrip = STDataManager.getInstance().getCurrentTrip();
		if(status == TripStatus.ON_TRIP_ADD) {
			int addedPassengers =  STDataManager.getInstance().getTempPassengerCount() - currentTrip.lastNumberOfPassengers;
			int addedLuggage =  STDataManager.getInstance().getTempLuggageCount() - currentTrip.lastNumberOfLuggage;
			message = "Gusto mo bang i-save ang\nPhp [[value]] para sa \n" + addedPassengers + " DAGDAG na pasahero at\n" + addedLuggage + " na bagahe?";
		}else if (status == TripStatus.ON_TRIP_DROP) {
			int droppedPassengers =  currentTrip.lastNumberOfPassengers - STDataManager.getInstance().getTempPassengerCount();
			int droppedLuggage = currentTrip.lastNumberOfLuggage - STDataManager.getInstance().getTempLuggageCount();
			message = "Gusto mo bang i-save ang\nPhp [[value]] para sa \n" + droppedPassengers + " BABABA na pasahero at\n" + droppedLuggage + " na bagahe?";
		}else if (status == TripStatus.END_TRIP) {
			int droppedPassengers =  currentTrip.lastNumberOfPassengers - STDataManager.getInstance().getTempPassengerCount();
			int droppedLuggage = currentTrip.lastNumberOfLuggage - STDataManager.getInstance().getTempLuggageCount();
			message = "Huling kustomer. Gusto mo bang i-save ang\nPhp [[value]] para sa \n" + droppedPassengers + " BABABA na pasahero at\n" + droppedLuggage + " na bagahe?";
		}else if (status == TripStatus.START_TRIP) {
			message = "Gusto mo bang i-save ang\nPhp [[value]] para sa\n" + STDataManager.getInstance().getTempPassengerCount() + " na pasahero at\n" + STDataManager.getInstance().getTempLuggageCount() + " na bagahe?";
		}else if (status == TripStatus.REFUEL) {
			message = "Gusto mo bang i-save ang\nPhp [[value]] para sa gas?";
		}
		mCalculator.setTitle(title);
		mCalculator.setAlertMessage(message);
		fm.beginTransaction().add(R.id.fullscreen_content, (Fragment)mCalculator, "calculator").commit();
	}
	
	private void hideCalculator() {
		//hide the calculator
		FragmentManager fm = getSupportFragmentManager();
		fm.beginTransaction().remove(mCalculator).commit();
	}
	
	private void hideActionButton() {
		mStartBtn.setVisibility(View.GONE);
		mStartBtn.setEnabled(false);
		mStartBtn.setClickable(false);
	}
	
	private void showActionButton() {
		mStartBtn.setVisibility(View.VISIBLE);
		mStartBtn.setEnabled(true);
		mStartBtn.setClickable(true);
	}
	
	private void showLuggageControl () {
		mLuggageControl.setVisibility(View.VISIBLE);
		mLuggageControl.setEnabled(true);
		mLuggageControl.setClickable(false);
	}
	
	private void hideLuggageControl () {
		mLuggageControl.setVisibility(View.GONE);
		mLuggageControl.setEnabled(false);
		mLuggageControl.setClickable(true);
	}

	
	
	
	
	
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            //showErrorDialog(connectionResult.getErrorCode());
        	Toast.makeText(this, "Error code: " + connectionResult.getErrorCode(), Toast.LENGTH_SHORT).show();
        }
		
	}

	@Override
	public void onConnected(Bundle dataBundle) {
		// Display the connection status
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        mLocationClient.requestLocationUpdates(mLocationRequest, this);
		
	}

	@Override
	public void onDisconnected() {
		// Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onLocationChanged(Location location) {
		
		//if the user is not loggedin, 
		//disable updates
		if(STDataManager.getInstance().currentUser == null)
			return;
		
		//Send the update to the current trip
		String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
		
		STDataManager.getInstance().updateLocation(location);
	}
	
	private void showLiterCalculator (TripStatus status, String title, CalculatorCallBack cb) {
		FragmentManager fm = this.getSupportFragmentManager();
		if(fm.findFragmentByTag("liter") != null)
			return;
		
		if(mLiterCalculator == null) {
			mLiterCalculator = new STCalculatorFragment();
		}
		mLiterCalculator.setCallBack(cb);
		
		String message =  "Gusto mo bang i-save ang\n [[value]] na litro ng gas?";

		mLiterCalculator.setTitle(title);
		mLiterCalculator.setAlertMessage(message);
		fm.beginTransaction().add(R.id.fullscreen_content, (Fragment)mLiterCalculator, "liter").commit();
	}
	
	private void hideLiterCalculator() {
		//hide the calculator
		FragmentManager fm = getSupportFragmentManager();
		fm.beginTransaction().remove(mLiterCalculator).commit();
	}
	
	
	private void showGasLiterInput() {
		showLiterCalculator(TripStatus.REFUEL, "Ilang Litro ng Gas?", new CalculatorCallBack () {

			@Override
			public void didCancel() {
				// TODO Auto-generated method stub
				hideLiterCalculator();
				mGasBtn.setVisibility(View.VISIBLE);
			}

			@Override
			public void didFinish(float amount) {
				// TODO Auto-generated method stub
				hideLiterCalculator();
				Log.d(TAG, "input liters: " + amount);
				showGasAmountInput(amount);
			}
			
		});
	}
	
	private void showGasAmountInput(final float liters) {
		showCalculator(TripStatus.REFUEL, "Magkano ang halaga ng binayaran sa Gas?", new CalculatorCallBack () {

			@Override
			public void didCancel() {
				// TODO Auto-generated method stub
				hideCalculator();
				mGasBtn.setVisibility(View.VISIBLE);
			}

			@Override
			public void didFinish(float amount) {
				// TODO Auto-generated method stub
				Log.d(TAG, "input amount: " + amount);
				mGasBtn.setVisibility(View.VISIBLE);
				STDataManager.getInstance().refuel(liters, amount);
				hideCalculator();
			}	
		});
	}
	
	private void showAlertDialog(Button sender) {
		
		boolean showCalculator = false;
		String title = "";
		String message = "";
		if(sender.equals(this.mGasBtn)) {
			title = "Gusto mo bang magpa-gas?";
			showCalculator = true;
		}else if(sender.equals(this.mRestBtn)) {
			title = "Gusto mo bang magpahinga?";
		}else if(sender.equals(this.mStartBtn)) {
			title = "Gusto mo bang tapusin ang iyong break?";
		}
		
		final boolean shouldShowCalc = showCalculator;
		final boolean shouldEndBreak = sender.equals(this.mStartBtn);
		
		AlertDialog.Builder ab = new AlertDialog.Builder(STMainFragmentActivity.this);
		ab.setTitle(title);
		ab.setNegativeButton("HINDI", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				
			}
		});
		ab.setPositiveButton("OO", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				
				if(shouldShowCalc) {
					mGasBtn.setVisibility(View.GONE);
					showGasLiterInput();
					dialog.dismiss();
				}else {
					dialog.dismiss();
					if(!shouldEndBreak) {
						STDataManager.getInstance().rest();
						updateButton(TripStatus.ON_BREAK);
						mRestBtn.setVisibility(View.GONE);
					}else {
						STDataManager.getInstance().endBreak();
						mRestBtn.setVisibility(View.VISIBLE);
						updateButton(TripStatus.IDLE);
						mProgressHUD = ProgressHUD.show(STMainFragmentActivity.this,"Recording data...", true,false,null);
						STDataManager.getInstance().sendRecentDataToServer(new STDataManagerPostDataCallBack () {

							@Override
							public void onResult(boolean result) {
								//go idle again
								updateButton(TripStatus.IDLE);
								mProgressHUD.dismiss();
							}
							
						});
	
					}
				}
			}
		});	
		
		ab.show();
	}
}

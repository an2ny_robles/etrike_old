package com.johndometita.moresecuretrike.fragment;

import com.johndometita.moresecuretrike.R;
import com.johndometita.moresecuretrike.managers.STDataManager;
import com.johndometita.moresecuretrike.managers.STDataManager.STDataManagerCallBack;
import com.johndometita.moresecuretrike.model.STUser;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class STLoginFragment extends Fragment {
	
	private Button mLoginBtn;
	private EditText mUserNameTextView;
	private EditText mPasswordTextView;
	
	public interface STLoginCallBack {
		void loginSuccess(STUser user);
		void loginFailed(String errorMessage);
	}

	private STLoginCallBack mCB;
	
	public STLoginFragment() {
		// TODO Auto-generated constructor stub
	}
	
	public void setCallBack (STLoginCallBack cb) {
		this.mCB = cb;
	}
	
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_login, container, false);
		
		mLoginBtn = (Button)view.findViewById(R.id.btn_login);
		mUserNameTextView = (EditText)view.findViewById(R.id.textView_username);
		mPasswordTextView = (EditText)view.findViewById(R.id.textView_password);
		
		
		
		mLoginBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mUserNameTextView.length() == 0) {
					mUserNameTextView.setError("Must not be empty");
					return;
				}
				
				if(mPasswordTextView.length() < 6) {
					mPasswordTextView.setError("Must be at least 6 characters");
					return;
				}
				
				STDataManager.getInstance().login(mUserNameTextView.getText().toString(), mPasswordTextView.getText().toString(), new STDataManagerCallBack () {

					@Override
					public void onResult(boolean success, String message) {
						if(success) {
							if(mCB != null)
								mCB.loginSuccess(STDataManager.getInstance().currentUser);
						}else {
							if(mCB != null)
								mCB.loginFailed(message);
						}
					}
					
				});
			}
			
		});
		return view;
	}

}

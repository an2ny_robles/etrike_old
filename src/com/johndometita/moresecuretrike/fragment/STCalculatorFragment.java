package com.johndometita.moresecuretrike.fragment;

import com.johndometita.moresecuretrike.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class STCalculatorFragment extends Fragment implements OnClickListener {
	
	public interface CalculatorCallBack {
		public void didCancel();
		public void didFinish(float amount);
	}
	
	private CalculatorCallBack mCB;
	private Button mBtn_0;
	private Button mBtn_1;
	private Button mBtn_2;
	private Button mBtn_3;
	private Button mBtn_4;
	private Button mBtn_5;
	private Button mBtn_6;
	private Button mBtn_7;
	private Button mBtn_8;
	private Button mBtn_9;
	private Button mBtn_delete;
	private Button mBtn_cancel;
	private Button mBtn_save;
	private Button mBtn_period;
	
	private TextView mTitleLabel;
	private TextView mAmountLabel;
	
	private String title;
	private String alertMessage;
	
	
	public STCalculatorFragment() {
		// TODO Auto-generated constructor stub
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setAlertMessage(String message) {
		this.alertMessage = message;
	}

	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	
		View view = inflater.inflate(R.layout.fragment_calculator, container, false);
		mBtn_0 = (Button)view.findViewById(R.id.calculator_btn_0);
		mBtn_1 = (Button)view.findViewById(R.id.calculator_btn_1);
		mBtn_2 = (Button)view.findViewById(R.id.calculator_btn_2);
		mBtn_3 = (Button)view.findViewById(R.id.calculator_btn_3);
		mBtn_4 = (Button)view.findViewById(R.id.calculator_btn_4);
		mBtn_5 = (Button)view.findViewById(R.id.calculator_btn_5);
		mBtn_6 = (Button)view.findViewById(R.id.calculator_btn_6);
		mBtn_7 = (Button)view.findViewById(R.id.calculator_btn_7);
		mBtn_8 = (Button)view.findViewById(R.id.calculator_btn_8);
		mBtn_9 = (Button)view.findViewById(R.id.calculator_btn_9);
		
		mBtn_delete = (Button)view.findViewById(R.id.calculator_btn_delete);
		mBtn_cancel = (Button)view.findViewById(R.id.calculator_btn_cancel);
		mBtn_save = (Button)view.findViewById(R.id.calculator_btn_save);
		mBtn_period = (Button)view.findViewById(R.id.calculator_btn_period);
		
		mTitleLabel = (TextView)view.findViewById(R.id.calculator_title);
		mAmountLabel = (TextView)view.findViewById(R.id.calculator_amount_label);
		
		mAmountLabel.setText("0");
		mTitleLabel.setText(title);
		
		mBtn_0.setOnClickListener(this);
		mBtn_1.setOnClickListener(this);
		mBtn_2.setOnClickListener(this);
		mBtn_3.setOnClickListener(this);
		mBtn_4.setOnClickListener(this);
		mBtn_5.setOnClickListener(this);
		mBtn_6.setOnClickListener(this);
		mBtn_7.setOnClickListener(this);
		mBtn_8.setOnClickListener(this);
		mBtn_9.setOnClickListener(this);
		
		mBtn_delete.setOnClickListener(this);
		mBtn_cancel.setOnClickListener(this);
		mBtn_save.setOnClickListener(this);
		mBtn_period.setOnClickListener(this);
		
		return view;
	}

	public CalculatorCallBack getCallBack() {
		return mCB;
	}

	public void setCallBack(CalculatorCallBack mCB) {
		this.mCB = mCB;
	}

	@Override
	public void onClick(View view) {
		if(alertMessage == null || alertMessage.isEmpty()) {
			Toast.makeText(getActivity(), "Err. Please try again.", Toast.LENGTH_SHORT).show();
			return;
		}
		String s = this.mAmountLabel.getText().toString();
		int length = s.length();
		if(view.equals(mBtn_cancel)) {
			this.mCB.didCancel();
			return;
		}else if(view.equals(mBtn_delete)) {
			if(length > 1) {
				s = s.substring(0, length - 1);
			}else {
				s = "";
			}
			this.mAmountLabel.setText(s);
			return;
		}else if(view.equals(mBtn_save)) {
			if(length == 0 || s == null) {
				Toast.makeText(getActivity(), "Invalid value. Please input the correct amount.", Toast.LENGTH_SHORT).show();
				return;
			}else {
				float val = 0;
				try {
					val =  Float.parseFloat(s);
				}catch (NumberFormatException e) {
					Toast.makeText(getActivity(), "Invalid value. Please input the correct amount.", Toast.LENGTH_SHORT).show();
					return;
				}
				final float value = val;
				AlertDialog.Builder ab = new AlertDialog.Builder(this.getActivity());
				ab.setTitle("ATTENTION");
				ab.setMessage(alertMessage.replace("[[value]]", value + ""));
				ab.setPositiveButton("OO", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mCB.didFinish(value);
					}
				});
				
				ab.setNegativeButton("HINDI", new DialogInterface.OnClickListener() {		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						//just do nothing
					}
				});
				
				ab.create().show();
			}
			return;
		}else if(view.equals(mBtn_0)) {
			s += "0";
		}else if(view.equals(mBtn_1)) {
			s += "1";
		}else if(view.equals(mBtn_2)) {
			s += "2";
		}else if(view.equals(mBtn_3)) {
			s += "3";
		}else if(view.equals(mBtn_4)) {
			s += "4";
		}else if(view.equals(mBtn_5)) {
			s += "5";
		}else if(view.equals(mBtn_6)) {
			s += "6";
		}else if(view.equals(mBtn_7)) {
			s += "7";
		}else if(view.equals(mBtn_8)) {
			s += "8";
		}else if(view.equals(mBtn_9)) {
			s += "9";
		}else if(view.equals(mBtn_period)) {
			if(!s.contains("."))
				s += ".";
		}
		if(s.length() > 7) return;
		
		this.mAmountLabel.setText(s);
	}
}

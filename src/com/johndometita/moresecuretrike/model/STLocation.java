package com.johndometita.moresecuretrike.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.johndometita.moresecuretrike.util.STSQLiteHelper;

/**
 * Represents the location that will be recorded on the device and on server
 * 
 * @author John Q. Dometita [john.dometita@gmail.com]
 */
public class STLocation {
	
	public enum TripStatus {
		/**
		 * Driver has no current trip, maybe either moving or just stationary
		 */
		IDLE,
		/**
		 * Driver is about to start a trip
		 */
		START_TRIP,
		/**
		 * Driver is currently on a trip
		 */
		ON_TRIP,
		/**
		 * Driver has dropped a passenger and still has others to service
		 */
		ON_TRIP_DROP,
		/**
		 * Driver has picked additional passenger and/or luggage
		 */
		ON_TRIP_ADD,
		/**
		 * Driver has serviced all passengers.
		 */
		END_TRIP,
		/**
		 * Driver stops for refuelling/charging
		 */
		REFUEL,
		/**
		 * Driver is on a break. The driver might be moving or stationary
		 */
		ON_BREAK
	}
	
	/**
	 * The status when the location is created
	 */
	public TripStatus status = TripStatus.IDLE;
	
	/**
	 * The latitude value when the STLocation instance is created
	 */
	private double latitude = 0.f;
	
	/**
	 * The longitude value when the STLocation instance is created
	 */
	private double longitude = 0.f;
	
	/**
	 * The confidence value of the latitude and longitude when the STLocation instance is created
	 */
	private float accuracy = 0.f;
	
	/**
	 * The fare when the STLocation instance is created
	 */
	private float fare = 0.f;
	
	/**
	 * The number of passengers when the STLocation instance is created
	 * This will only be recorded when Status is START, END, ON_TRIP_DROP, ON_TRIP_ADD
	 */
	private int passengerCount = 0;
	
	/**
	 * The number of luggage when the STLocation instance is created
	 */
	private int luggageCount = 0;

	private long timestamp;

	private String provider;
	
	public int internalId;


	public STLocation() {
		// TODO Auto-generated constructor stub
	}
	
	public void setLuggageCount(int count) {
		if(count < 0)
			count = 0;
		
		switch(status) {
		case ON_TRIP:
		case START_TRIP:
		case ON_TRIP_DROP:
		case ON_TRIP_ADD:
			luggageCount = count;
			break;
		default:
			luggageCount = 0;
		}
	}
	
	public int getLuggageCount() {
		return luggageCount;
	}
	
	public void setPassengerCount(int count) {
		if(count < 0)
			count = 0;
		
		switch(status) {
		case ON_TRIP:
		case START_TRIP:
		case ON_TRIP_DROP:
		case ON_TRIP_ADD:
			passengerCount = count;
			break;
		default:
			passengerCount = 0;
		}
	}
	
	public int getPassengerCount() {
		return passengerCount;
	}
	
	
	public void setFare(float value) {
		
		switch(status) {
		case END_TRIP:
		case START_TRIP:
		case ON_TRIP_DROP:
		case ON_TRIP_ADD:
			fare = value;
			break;
		default:
			fare = 0;
		}
		
	}
	
	public float getFare() {
		return fare;
	}
	
	
	public void setLongitude(double value) {
		if(value < 0.f)
			value = 0.f;
		
		longitude = value;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void setLatitude(double value) {
		if(value < 0.f)
			value = 0.f;
		
		latitude = value;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setAccuracy (float value) {
		this.accuracy = value;
	}
	
	public float getAccuracy () {
		return this.accuracy;
	}
	
	public static String getStatusAsString(TripStatus status) {
		String text = "";
		switch (status) {
		case START_TRIP:
			text = "START TRIP";
			break;
		case ON_TRIP:
			text = "ON A TRIP";
			break;
		case ON_TRIP_DROP:
			text = "DROPPING A PASSENGER";
			break;
		case ON_TRIP_ADD:
			text = "ADDING A PASSENGER";
			break;
		case END_TRIP:
			text = "END TRIP";
			break;
		case REFUEL:
			text = "REFUEL";
			break;
		case ON_BREAK:
			text = "ON A BREAK";
			break;
		default:
			text = "IDLE";
			break;			
		}
		return text;
	}

	public void setTimestamp(long time) {
		this.timestamp = time;
	}
	
	public long getTimestamp () {
		return this.timestamp;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	public String getProvider () {
		return this.provider;
	}
	
	public String toJSONString () throws JSONException {
		JSONObject location = new JSONObject();
		location.put(STSQLiteHelper.COL_LAT, this.getLatitude());
		location.put(STSQLiteHelper.COL_LON, this.getLongitude());
		location.put(STSQLiteHelper.COL_ACCURACY, this.getAccuracy());
		location.put(STSQLiteHelper.COL_FARE, this.getFare());
		location.put(STSQLiteHelper.COL_PASSENGER, this.getPassengerCount());
		location.put(STSQLiteHelper.COL_LUGGAGE, this.getLuggageCount());
		location.put(STSQLiteHelper.COL_TIMESTAMP, this.getTimestamp());
		location.put(STSQLiteHelper.COL_PROVIDER, this.getProvider());
		location.put(STSQLiteHelper.COL_STATUS, this.getStatusAsString(status));
		return location.toString();
	}
}

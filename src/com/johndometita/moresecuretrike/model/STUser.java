package com.johndometita.moresecuretrike.model;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

public class STUser {

	private int userId;
	private String firstName;
	private String lastName;
	private String password;
	private String deviceId;
	private double homeLat;
	private double homeLon;
	private String address;
	private double garageLat;
	private double garageLon;
	private double terminal1Lat;
	private double terminal1Lon;
	private double terminal2Lat;
	private double terminal2Lon;
	private double terminal3Lat;
	private double terminal3Lon;
	private String userName;
	
	public static String USER_ID = "user_id";
	public static String FIRST_NAME = "first_name";
	public static String DEVICE_ID = "device_id";
	
	public boolean hasGarage() {
		return !(garageLat < 0 || garageLon < 0);
	}
	
	public boolean hasHome() {
		return !(homeLat < 0 || homeLon < 0);
	}
	
	public boolean hasTerminal1() {
		return !(terminal1Lat < 0 || terminal1Lon < 0);
	}
	
	public boolean hasTerminal2() {
		return !(terminal2Lat < 0 || terminal2Lon < 0);
	}
	
	public boolean hasTerminal3() {
		return !(terminal3Lat < 0 || terminal3Lon < 0);
	}
	
	public STUser() {
		// TODO Auto-generated constructor stub
		this.homeLat = -1;
		this.homeLon = -1;
		this.garageLat = -1;
		this.garageLon  = -1;
		this.terminal1Lat = -1;
		this.terminal1Lon = -1;
		this.terminal2Lat = -1;
		this.terminal2Lon = -1;
		this.terminal3Lat = -1;
		this.terminal3Lon = -1;
	}

	public int getUserId() {
		// TODO Auto-generated method stub
		return this.userId;
	}

	public String getDeviceId() {
		// TODO Auto-generated method stub
		return this.deviceId;
	}

	public void setUserId(int i) {
		this.userId = i;
		
	}

	public void setDeviceId(String string) {
		
		//ignore this
		this.deviceId = string;
	}

	public String getUserName() {
		
		return this.userName;
	}

	public void setUserName(String string) {
		this.userName = string;
	}

	public void setLandmarkLocation(String landmarkName,
			STLocation mCurrentLocation) {
		Log.d("setLandmarkLocation", landmarkName);
		if(landmarkName.equals("HOME")) {
			this.homeLat = mCurrentLocation.getLatitude();
			this.homeLon = mCurrentLocation.getLongitude();
		}else if(landmarkName.equals("GARAGE")) {
			this.garageLat = mCurrentLocation.getLatitude();
			this.garageLon = mCurrentLocation.getLongitude();
		}else if(landmarkName.equals("TERMINAL_1")) {
			this.terminal1Lat = mCurrentLocation.getLatitude();
			this.terminal1Lon = mCurrentLocation.getLongitude();
		}else if(landmarkName.equals("TERMINAL_2")) {
			this.terminal2Lat = mCurrentLocation.getLatitude();
			this.terminal2Lon = mCurrentLocation.getLongitude();
		}else if(landmarkName.equals("TERMINAL_3")) {
			this.terminal3Lat = mCurrentLocation.getLatitude();
			this.terminal3Lon = mCurrentLocation.getLongitude();
		}
	}

	


}

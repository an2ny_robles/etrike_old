package com.johndometita.moresecuretrike.model;

import android.util.Log;

import com.johndometita.moresecuretrike.model.STLocation.TripStatus;

public class STTrip {

	public int lastNumberOfPassengers = 0;
	public int lastNumberOfLuggage = 0;
	public float totalFare = 0.f;
	public int totalPassengerCount;
	public int totalLuggageCount = 0;

	private String TAG = "STTrip";
	public STTrip() {
		
	}
	
	public void startTrip(STLocation startLocation) {
		lastNumberOfPassengers = startLocation.getPassengerCount();
		lastNumberOfLuggage = startLocation.getLuggageCount();
		totalFare += startLocation.getFare();
		totalPassengerCount += startLocation.getPassengerCount();
		totalLuggageCount += startLocation.getLuggageCount();
		Log.d(TAG, "Starting trip with fare: " + startLocation.getFare() + " passengers: " + startLocation.getPassengerCount() + " luggage: " + startLocation.getLuggageCount());
		
		saveToDB(startLocation);
	}
	
	public void endTrip(STLocation endLocation) {
		lastNumberOfPassengers = endLocation.getPassengerCount();
		lastNumberOfLuggage = endLocation.getLuggageCount();
		totalFare += endLocation.getFare();
		
		saveToDB(endLocation);
	}
	
	public void updateTrip(STLocation updateLocation) {
		Log.d(TAG, updateLocation.status +" update trip with fare: " + updateLocation.getFare() + " passengers: " + updateLocation.getPassengerCount() + " luggage: " + updateLocation.getLuggageCount());
		lastNumberOfPassengers = updateLocation.getPassengerCount();
		lastNumberOfLuggage = updateLocation.getLuggageCount();
	
		TripStatus s = updateLocation.status;
		//update values only in ON_TRIP_XXX status
		if(s == TripStatus.ON_TRIP ||  s == TripStatus.ON_TRIP_ADD || s == TripStatus.ON_TRIP_DROP) {
			totalFare += updateLocation.getFare();
			int diff = updateLocation.getPassengerCount() - totalPassengerCount;
			if(diff > 0)
				totalPassengerCount += diff;
			
			diff = updateLocation.getLuggageCount() - totalLuggageCount;
			if(diff > 0)
				totalLuggageCount += diff;
		}
		
		saveToDB(updateLocation);
	}
	
	private void saveToDB (STLocation location) {
		
	}
	
	
}

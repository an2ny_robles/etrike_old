package com.johndometita.moresecuretrike.util;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.johndometita.moresecuretrike.model.STLocation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class STSQLiteHelper extends SQLiteOpenHelper {
	
	/**
	 * Database version
	 */
	private static final int DATABASE_VERSION = 1;
	
	private static final String TABLE_LOCATIONS = "locations";
	public static final String COL_ID = "id";
	public static final String COL_LAT = "lat";
	public static final String COL_LON = "lon";
	public static final String COL_ACCURACY = "accuracy";
	public static final String COL_FARE = "fare";
	public static final String COL_PASSENGER = "passengers";
	public static final String COL_LUGGAGE = "luggage";
	public static final String COL_TIMESTAMP = "timestamp";
	public static final String COL_PROVIDER = "provider";
	public static final String COL_STATUS = "status";
	
	private static final String[] COLUMNS = {COL_ID, COL_LAT, COL_LON, COL_ACCURACY, COL_FARE, COL_PASSENGER, COL_LUGGAGE, COL_TIMESTAMP, COL_PROVIDER, COL_PROVIDER};
	
	/**
	 * Database Name
	 */
	private static final String DATABASE_NAME = "MoreSecureTrikeDB";
	/**
	 * 
	 * @param context
	 * @param name
	 * @param factory
	 * @param version
	 */
	public STSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LOCATION_TABLE = "CREATE TABLE "+ TABLE_LOCATIONS  +" (" +
					"id INTEGER PRIMARY KEY AUTOINCREMENT, " + 
					"lat REAL, " +
					"lon REAL, " +
					"accuracy REAL, " +
					"fare REAL, " +
					"passengers INTEGER, " +
					"luggage INTEGER, " +
					"timestamp INTEGER, " +
					"provider TEXT, " +
					"status TEXT)";
		db.execSQL(CREATE_LOCATION_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS locations");
        // create fresh books table
        this.onCreate(db);
	}
	
	public void addSTLocation (STLocation location) {
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(COL_LAT, location.getLatitude());
		values.put(COL_LON, location.getLongitude());
		values.put(COL_ACCURACY, location.getAccuracy());
		values.put(COL_FARE, location.getFare());
		values.put(COL_PASSENGER, location.getPassengerCount());
		values.put(COL_LUGGAGE, location.getLuggageCount());
		values.put(COL_TIMESTAMP, location.getTimestamp());
		values.put(COL_PROVIDER, location.getProvider());
		values.put(COL_STATUS, STLocation.getStatusAsString(location.status));
		
		db.insert(TABLE_LOCATIONS, null, values);
		db.close();
	}
	
	public void deleteLocationsFrom (int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_LOCATIONS,
				COL_ID + " <= ?",
				new String [] { id + ""});
		db.close();
	}
	
	public JSONObject getAllLocations () throws JSONException {
		JSONObject object = new JSONObject ();
		
		String query = "SELECT * FROM " + TABLE_LOCATIONS;
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		JSONArray locations = new JSONArray();
		JSONObject location = null;
		int lastId = 0;
		if(!db.isOpen()) {
			db = this.getReadableDatabase();
		}
		if(cursor.moveToFirst()) {
			do {
				location = new JSONObject();
				lastId = cursor.getInt(0);
				location.put(COL_LAT, cursor.getDouble(1));
				location.put(COL_LON, cursor.getDouble(2));
				location.put(COL_ACCURACY, cursor.getFloat(3));
				location.put(COL_FARE, cursor.getFloat(4));
				location.put(COL_PASSENGER, cursor.getInt(5));
				location.put(COL_LUGGAGE, cursor.getInt(6));
				location.put(COL_TIMESTAMP, cursor.getLong(7));
				location.put(COL_PROVIDER, cursor.getString(8));
				location.put(COL_STATUS, cursor.getString(9));
				locations.put(location);
			}while (cursor.moveToNext());
		}
		
		object.put("locations", locations);
		object.put("lastId", lastId);
		db.close();
		return object;
	}

}

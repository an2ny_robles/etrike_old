package com.johndometita.moresecuretrike.managers;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Button;

import com.google.android.gms.location.LocationRequest;
import com.johndometita.moresecuretrike.model.STLocation;
import com.johndometita.moresecuretrike.model.STLocation.TripStatus;
import com.johndometita.moresecuretrike.model.STTrip;
import com.johndometita.moresecuretrike.model.STUser;
import com.johndometita.moresecuretrike.util.STSQLiteHelper;
import com.turbomanage.httpclient.AsyncCallback;
import com.turbomanage.httpclient.HttpResponse;
import com.turbomanage.httpclient.ParameterMap;
import com.turbomanage.httpclient.android.AndroidHttpClient;

public class STDataManager {

	private static String BASE_URL = "http://secure-trike.com:3000";//"http://54.201.108.176:3000";//"http://192.168.43.231:3000";
	//private static String BASE_URL = "http://192.168.43.231:3000";
	private static String API_URL = "/api";
	
	public STDataManager() {
		// TODO Auto-generated constructor stub
	}
	
	public STUser currentUser;
	private static STDataManager mInstance;
	private STLocation mCurrentLocation;
	private Location currentLocation;
	private TripStatus currentStatus = TripStatus.IDLE;
	private TripStatus previousStatus = TripStatus.IDLE;
	public TripStatus futureStatus = TripStatus.IDLE;
	private int tempPassengerCount = 0;
	private int tempLuggageCount = 0;
	private STTrip mCurrentTrip;
	private static String TAG = "STDataManager";
	private STSQLiteHelper db;
	private String deviceId;
	private static String PREFERENCE_FILE_NAME = "STPreferences";
	private Context mCtx;
	private int mCounter = 0;
	private boolean mSubmittingData = false;
	private SharedPreferences mPreferences;
	
	public interface STDataManagerCallBack {
		void onResult(boolean success, String message);
	}
	
	public interface STDataManagerLandmarkCallBack {
		void onResult(JSONArray landmarks);
	}

	private static SharedPreferences getSharedPreferencesMethod(Context context) {
		return context.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
	}
	
	private void saveCredentials(String username, String password) {
		//this should have been hashed at least, but to our current needs, no need
		//getSharedPreferencesMethod(mCtx).edit().putString("username", username).commit();
		//getSharedPreferencesMethod(mCtx).edit().putString("password", password).commit();
		SharedPreferences.Editor e = mPreferences.edit();
	    e.clear();
	    e.putString("username", username);
	    e.putString("password", password);
	    e.commit();		
	}
	

	public static String getIMEI(Context ctx)
	{
		TelephonyManager tm = (TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE);
		if(tm==null){
			System.out.println("Device TM: "+tm);
		}		
		return tm.getDeviceId();	
	}
	
	public static STDataManager startInstance(Context ctx) {
		
		if(mInstance == null) {
			mInstance = new STDataManager();
		}else {
			return mInstance;
		}
		
		mInstance.mCtx = ctx;	
		mInstance.db = new STSQLiteHelper(ctx);
		mInstance.deviceId = getIMEI(ctx);
		mInstance.mPreferences = getSharedPreferencesMethod(ctx);

		return mInstance;
	}
	
	public static STDataManager getInstance () {
		//Log.d(TAG, "IMEI  " + mInstance.currentUser.getDeviceId());
		return mInstance;
	}
		
	public STLocation getCurrentLocation () {
		if(mCurrentLocation == null) {
			mCurrentLocation = new STLocation();
		}
		
		insertLocationDataToSTLocation(currentLocation);
		return mCurrentLocation;
	}
	
	public void createNewTrip (int numberOfPassengers, int numberOfLuggage, float initialFare) {
		mCurrentLocation = new STLocation();
		currentStatus = TripStatus.START_TRIP;
		mCurrentLocation.status = TripStatus.START_TRIP;
		
		insertLocationDataToSTLocation(currentLocation);
		mCurrentLocation.setFare(initialFare);
		mCurrentLocation.setLuggageCount(numberOfLuggage);
		mCurrentLocation.setPassengerCount(numberOfPassengers);
		
		this.writeToDB(mCurrentLocation);
		
		
		mCurrentTrip = new STTrip();
		mCurrentTrip.startTrip(mCurrentLocation);
		currentStatus = TripStatus.ON_TRIP;
		mCurrentLocation.status = TripStatus.ON_TRIP;
	}
	
	
	public TripStatus getCurrentStatus () {
		return currentStatus;
	}
	
	public void setTempPassengerCount (int value) {
		if(value < 0)
			value = 0;
		
		tempPassengerCount = value;
	}
	
	public int getTempPassengerCount() {
		return tempPassengerCount;
	}
	
	public void setTempLuggageCount (int value) {
		if(value < 0)
			value = 0;
		
		tempLuggageCount = value;
	}
	
	public int getTempLuggageCount() {
		return tempLuggageCount;
	}
	
	public void endTrip (int numberOfPassengers, int numberOfLuggage, float fare) {
		if(mCurrentTrip == null) return;
		
		STLocation update = new STLocation();
		currentStatus = TripStatus.ON_TRIP_DROP;
		update.status = TripStatus.ON_TRIP_DROP;
		update.setFare(fare);
		update.setLuggageCount(mCurrentLocation.getLuggageCount()); //get last
		update.setPassengerCount(mCurrentLocation.getPassengerCount()); //get last
		mCurrentLocation = update;
		insertLocationDataToSTLocation(currentLocation);
		mCurrentTrip.updateTrip(mCurrentLocation);
		
		currentStatus = TripStatus.END_TRIP;
		mCurrentLocation.status = TripStatus.END_TRIP;
		
		//TODO: save!
		this.writeToDB(mCurrentLocation);
		
		
		mCurrentTrip = null;
		currentStatus = TripStatus.IDLE;
		futureStatus = TripStatus.IDLE;
		previousStatus = TripStatus.IDLE;
		mCurrentLocation = new STLocation();
		insertLocationDataToSTLocation(currentLocation);

	}
	
	public void refuel(float liters, float amount) {
		previousStatus  = currentStatus;
		currentStatus = TripStatus.REFUEL;
		mCurrentLocation = new STLocation();
		mCurrentLocation.status = TripStatus.REFUEL;
		insertLocationDataToSTLocation(currentLocation);
		
		if(mCurrentTrip != null) {
			mCurrentTrip.updateTrip(mCurrentLocation);
		}
	
		this.writeToDB(mCurrentLocation);
		
		//send data to server
		sendGasDataToServer(mCurrentLocation, liters, amount);
		currentStatus = previousStatus;
		mCurrentLocation.status = previousStatus;
	}
	
	private void sendGasDataToServer (STLocation location, float liters, float amount) {
		
		AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);
        ParameterMap params = new ParameterMap();
        params.add(STUser.USER_ID, currentUser.getUserId() + "");
        params.add(STUser.DEVICE_ID, this.deviceId);
        params.add("liters", liters + "");
        params.add("amount", amount + "");
        try {
			params.add("location", location.toJSONString());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
 
        httpClient.post(API_URL + "/submit/gas", params, new AsyncCallback() {
			@Override
			public void onComplete(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onError(Exception e) {
		        e.printStackTrace();
		    }
        });
	}
	
	public void getUserLandmarks (final STDataManagerLandmarkCallBack cb) {
		
		if(currentUser == null) {
			if(cb != null)
				cb.onResult(null);
			return; 
		}
		AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);
        httpClient.get(API_URL + "/landmark?user_id=" + currentUser.getUserId(), null, new AsyncCallback() {

			@Override
			public void onComplete(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				if(httpResponse.getStatus() != 200) {
					if(cb != null)
						cb.onResult(null);
					return;	
				}
				
				JSONArray landmarks = null;
			
				try {
					landmarks = new JSONArray(httpResponse.getBodyAsString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (int i=0; i < landmarks.length(); i++){
					try {
						JSONObject landmark = landmarks.getJSONObject(i);
						STLocation loc = new STLocation();
						loc.setLatitude(landmark.getDouble("lat"));
						loc.setLongitude(landmark.getDouble("lon"));
						currentUser.setLandmarkLocation(landmark.getString("type"), loc);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(cb != null) {
					cb.onResult(landmarks);
				}
			}
			
			public void onError(Exception e) {
		        e.printStackTrace();
		        if(cb != null)
					cb.onResult(null);
		    }
        	
        });
	}
	
	public void sendLandmarkDataToServer (final String landmarkName, final STDataManagerCallBack cb) {
		
		if(mCurrentLocation == null) {
			if(cb != null)
				cb.onResult(false, "Your location has not yet been set. Please wait for a while then try again.");
			return;
		}
		
		AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);
        ParameterMap params = new ParameterMap();
        params.add(STUser.USER_ID, currentUser.getUserId() + "");
        params.add(STUser.DEVICE_ID, this.deviceId);
        params.add("landmark", landmarkName);
        try {
			params.add("location", mCurrentLocation.toJSONString());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
 
        httpClient.post(API_URL + "/submit/landmark", params, new AsyncCallback() {
			@Override
			public void onComplete(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				if(httpResponse.getStatus() != 200) {
					if(cb != null)
						cb.onResult(false, httpResponse.getBodyAsString());
					return;	
				}
				
				if(cb != null) {
					currentUser.setLandmarkLocation(landmarkName, mCurrentLocation);
					cb.onResult(true, "");
				}
			}
			
			public void onError(Exception e) {
		        e.printStackTrace();
		        if(cb != null)
					cb.onResult(false, "Something is not right. Please try again later or contact admin.");
		    }
        });
	}
	

	public void rest() {
		currentStatus = TripStatus.ON_BREAK;
		STLocation update = new STLocation();
		update.status = TripStatus.ON_BREAK;
		update.setFare(0);
		update.setLuggageCount(0);
		update.setPassengerCount(0);
		mCurrentLocation = update;
		insertLocationDataToSTLocation(currentLocation);
	
		this.writeToDB(mCurrentLocation);
	}
	
	public void endBreak () {
		currentStatus = TripStatus.IDLE;
		STLocation update = new STLocation();
		update.status = TripStatus.IDLE;
		update.setFare(0);
		update.setLuggageCount(0);
		update.setPassengerCount(0);
		mCurrentLocation = update;
		insertLocationDataToSTLocation(currentLocation);
	
		this.writeToDB(mCurrentLocation);
	}
	
	public void updateTrip (int numberOfPassengers, int numberOfLuggage, float fare, TripStatus status) {
		if(mCurrentTrip == null) return;
		//the currentstatus will still be ON_TRIP
		STLocation update = new STLocation();
		update.status = status;
		update.setFare(fare);
		update.setLuggageCount(numberOfLuggage);
		update.setPassengerCount(numberOfPassengers);
		mCurrentLocation = update;
		insertLocationDataToSTLocation(currentLocation);
		mCurrentTrip.updateTrip(mCurrentLocation);
		this.writeToDB(mCurrentLocation);
		
		//after writing to db, make it ON_TRIP again
		mCurrentLocation.status = TripStatus.ON_TRIP;
	}
	
	
	public STTrip getCurrentTrip () {
		return mCurrentTrip;
	}

	public void updateLocation(Location location) {
		currentLocation = location;
		if(mCurrentLocation == null) {
			mCurrentLocation = new STLocation();
		}
		
		this.insertLocationDataToSTLocation(location);
		if(mCurrentTrip != null)
			this.updateTrip(mCurrentLocation.getPassengerCount(), mCurrentLocation.getLuggageCount(), mCurrentLocation.getFare(), mCurrentLocation.status);
		else {
			this.writeToDB(mCurrentLocation);
		}
		
		mCounter++;
		if(mCounter > 360 && mCounter % 10 == 0) {
			//force update to server
			sendRecentDataToServer(new STDataManagerPostDataCallBack() {
				@Override
				public void onResult(boolean result) {
					// TODO Auto-generated method stub
					mCounter = 0;
				}
			});
		}	
	}
	
	private void insertLocationDataToSTLocation (Location location) {
		if(mCurrentLocation != null && location != null) {
			mCurrentLocation.setLatitude(location.getLatitude());
			mCurrentLocation.setLongitude(location.getLongitude());
			mCurrentLocation.setAccuracy(location.getAccuracy());
			mCurrentLocation.setTimestamp(location.getTime());
			mCurrentLocation.setProvider(location.getProvider());
		}
	}
	
	private void sendToServer(STLocation location) {
		Log.d(TAG, "sendToServer " + location.getLatitude() + " " + location.getLongitude() + " provider: " + location.getProvider());
		AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);
        ParameterMap params = new ParameterMap();
        params.add(STUser.USER_ID, currentUser.getUserId() + "");
        params.add(STUser.DEVICE_ID, currentUser.getDeviceId());
 
        httpClient.post(API_URL + "/submit/points", params, new AsyncCallback() {
			@Override
			public void onComplete(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				
			}
        });
	}
	
	private void writeToDB (STLocation location) {
		Log.d(TAG, "writeToDB " + location.getLatitude() + " " + location.getLongitude() + " provider: " + location.getProvider() + " accuracy: " + location.getAccuracy());
		if(location != null) {
			this.db.addSTLocation(location);
		}
	}
	
	public interface STDataManagerPostDataCallBack {
		void onResult(boolean result);
	}
	
	public JSONObject getData () {
		JSONObject data = null;
		try {
			data = db.getAllLocations();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return data;
	}
	
	public void sendRecentDataToServer (final STDataManagerPostDataCallBack cb) {
		Log.d(TAG, "sendRecentDataToServer ");
		if(mSubmittingData) {
			if(cb != null) {
				//data is currently being submitted
				cb.onResult(false);
			}
			return;
		}
		mSubmittingData = true;
		STLongSubmissionTask submitTask = new STLongSubmissionTask();
		submitTask.setCallBack(cb);
		submitTask.execute(new Void[] {});
	}
	
	public void autologin(final STDataManagerCallBack cb) {
		String username = mPreferences.getString("username", "");
		String password = mPreferences.getString("password", "");
		if(username.isEmpty() || password.isEmpty()) {
			cb.onResult(false, "No credentials yet. Please login on the login page.");
			return;
		}
		this.login(username, password, cb);
	}

	public void login(final String username, final String password, final STDataManagerCallBack cb) {
		AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);
        ParameterMap params = new ParameterMap();
        params.add("username", username);
        params.add("password", password);
        params.add("device_id", this.deviceId);
 
        httpClient.post(API_URL + "/login", params, new AsyncCallback() {
			@Override
			public void onComplete(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				if(httpResponse.getStatus() != 200) {
					cb.onResult(false, httpResponse.getBodyAsString());
					return;
				}
				
				//TODO: set the user here
				
				JSONObject object = null;
				try {
					object = new JSONObject(httpResponse.getBodyAsString());
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(object != null) {
					STUser user = new STUser();
					try {
						user.setUserId(object.getInt("user_id"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						user.setUserName(object.getString("username"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					mInstance.currentUser = user;
					//save the credentials
					saveCredentials(username, password);
					cb.onResult(true, "Welcome user!");
				}else {
					cb.onResult(false, "Something is not right. Please contact admin.");
				}
			}
			
			public void onError(Exception e) {
		        e.printStackTrace();
		        cb.onResult(false, "Something went wrong. Please check your internet connection. Or contact admin for immediate resolution.");
		    }
        });
	}
	
	 
	private class STLongSubmissionTask extends AsyncTask <Void, Void, JSONObject> {
		
		private STDataManagerPostDataCallBack cb;
		
		public void setCallBack (STDataManagerPostDataCallBack myCB) {
			this.cb = myCB;
		}

		@Override
		protected JSONObject doInBackground(Void... params) {
			// TODO Auto-generated method stub
			JSONObject object = null;
			try {
				object = db.getAllLocations();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return object;
		}
		
		@Override
	    protected void onPostExecute(JSONObject data) {
			if(data == null) {
				
			}else {
				
				if(currentUser == null) {
					cb.onResult(false);
					return;
		        }

				int lastId = 0;
				try {
					lastId = data.getInt("lastId");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String locationString = "";
				try {
					JSONArray locations = data.getJSONArray("locations");
					locationString = locations.toString();
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				final int lastID = lastId;

				AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
		        httpClient.setMaxRetries(5);
		        ParameterMap params = new ParameterMap();
		        
		        params.add(STUser.USER_ID, currentUser.getUserId() + "");
		        params.add(STUser.DEVICE_ID, deviceId);
		        params.add("locations", locationString);
		 
		        httpClient.post(API_URL + "/submit/points", params, new AsyncCallback() {
					@Override
					public void onComplete(HttpResponse httpResponse) {
						// TODO Auto-generated method stub
						if(httpResponse.getStatus() != 200) {
							mSubmittingData = false;
							cb.onResult(false);
							return;
						}
						
						//delete 
						db.deleteLocationsFrom(lastID);
						cb.onResult(true);
						mSubmittingData = false;
					}
					
					public void onError(Exception e) {
				        e.printStackTrace();
				        cb.onResult(false);
				        mSubmittingData = false;
				    }
		        });
			}
	    }

		
	}
}

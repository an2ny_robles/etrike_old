package com.johndometita.moresecuretrike.managers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.android.gms.maps.model.LatLng;
import com.turbomanage.httpclient.AsyncCallback;
import com.turbomanage.httpclient.android.AndroidHttpClient;


public class STLocationManager implements 
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,
LocationListener {
	
	private class GMapV2Direction {
	    public final static String MODE_DRIVING = "driving";
	    public final static String MODE_WALKING = "walking";

	    public GMapV2Direction() { }

	    public Document getDocument(LatLng start, LatLng end, String mode) {
	        String url = "http://maps.googleapis.com/maps/api/directions/xml?" 
	                + "origin=" + start.latitude + "," + start.longitude  
	                + "&destination=" + end.latitude + "," + end.longitude 
	                + "&sensor=false&units=metric&mode=driving";

	        try {
	            HttpClient httpClient = new DefaultHttpClient();
	            HttpContext localContext = new BasicHttpContext();
	            HttpPost httpPost = new HttpPost(url);
	            HttpResponse response = httpClient.execute(httpPost, localContext);
	            InputStream in = response.getEntity().getContent();
	            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	            Document doc = builder.parse(in);
	            return doc;
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return null;
	    }

	    public String getDurationText (Document doc) {
	        NodeList nl1 = doc.getElementsByTagName("duration");
	        Node node1 = nl1.item(nl1.getLength() - 1);
	        NodeList nl2 = node1.getChildNodes();
	        Node node2 = nl2.item(getNodeIndex(nl2, "text"));
	        Log.i("DurationText", node2.getTextContent());
	        return node2.getTextContent();
	    }

	    public int getDurationValue (Document doc) {
	        NodeList nl1 = doc.getElementsByTagName("duration");
	        Node node1 = nl1.item(nl1.getLength() - 1);
	        NodeList nl2 = node1.getChildNodes();
	        Node node2 = nl2.item(getNodeIndex(nl2, "value"));
	        Log.i("DurationValue", node2.getTextContent());
	        return Integer.parseInt(node2.getTextContent());
	    }

	    public String getDistanceText (Document doc) {
	        NodeList nl1 = doc.getElementsByTagName("distance");
	        Node node1 = nl1.item(nl1.getLength() - 1);
	        NodeList nl2 = node1.getChildNodes();
	        Node node2 = nl2.item(getNodeIndex(nl2, "text"));
	        Log.i("DistanceText", node2.getTextContent());
	        return node2.getTextContent();
	    }

	    public int getDistanceValue (Document doc) {
	        NodeList nl1 = doc.getElementsByTagName("distance");
	        Node node1 = nl1.item(nl1.getLength() - 1);
	        NodeList nl2 = node1.getChildNodes();
	        Node node2 = nl2.item(getNodeIndex(nl2, "value"));
	        Log.i("DistanceValue", node2.getTextContent());
	        return Integer.parseInt(node2.getTextContent());
	    }

	    public String getStartAddress (Document doc) {
	        NodeList nl1 = doc.getElementsByTagName("start_address");
	        Node node1 = nl1.item(0);
	        Log.i("StartAddress", node1.getTextContent());
	        return node1.getTextContent();
	    }

	    public String getEndAddress (Document doc) {
	        NodeList nl1 = doc.getElementsByTagName("end_address");
	        Node node1 = nl1.item(0);
	        Log.i("StartAddress", node1.getTextContent());
	        return node1.getTextContent();
	    }

	    public String getCopyRights (Document doc) {
	        NodeList nl1 = doc.getElementsByTagName("copyrights");
	        Node node1 = nl1.item(0);
	        Log.i("CopyRights", node1.getTextContent());
	        return node1.getTextContent();
	    }

	    public ArrayList<LatLng> getDirection (Document doc) {
	        NodeList nl1, nl2, nl3;
	        ArrayList<LatLng> listGeopoints = new ArrayList<LatLng>();
	        nl1 = doc.getElementsByTagName("step");
	        if (nl1.getLength() > 0) {
	            for (int i = 0; i < nl1.getLength(); i++) {
	                Node node1 = nl1.item(i);
	                nl2 = node1.getChildNodes();

	                Node locationNode = nl2.item(getNodeIndex(nl2, "start_location"));
	                nl3 = locationNode.getChildNodes();
	                Node latNode = nl3.item(getNodeIndex(nl3, "lat"));
	                double lat = Double.parseDouble(latNode.getTextContent());
	                Node lngNode = nl3.item(getNodeIndex(nl3, "lng"));
	                double lng = Double.parseDouble(lngNode.getTextContent());
	                listGeopoints.add(new LatLng(lat, lng));

	                locationNode = nl2.item(getNodeIndex(nl2, "polyline"));
	                nl3 = locationNode.getChildNodes();
	                latNode = nl3.item(getNodeIndex(nl3, "points"));
	                ArrayList<LatLng> arr = decodePoly(latNode.getTextContent());
	                for(int j = 0 ; j < arr.size() ; j++) {
	                    listGeopoints.add(new LatLng(arr.get(j).latitude, arr.get(j).longitude));
	                }

	                locationNode = nl2.item(getNodeIndex(nl2, "end_location"));
	                nl3 = locationNode.getChildNodes();
	                latNode = nl3.item(getNodeIndex(nl3, "lat"));
	                lat = Double.parseDouble(latNode.getTextContent());
	                lngNode = nl3.item(getNodeIndex(nl3, "lng"));
	                lng = Double.parseDouble(lngNode.getTextContent());
	                listGeopoints.add(new LatLng(lat, lng));
	            }
	        }

	        return listGeopoints;
	    }

	    private int getNodeIndex(NodeList nl, String nodename) {
	        for(int i = 0 ; i < nl.getLength() ; i++) {
	            if(nl.item(i).getNodeName().equals(nodename))
	                return i;
	        }
	        return -1;
	    }

	    private ArrayList<LatLng> decodePoly(String encoded) {
	        ArrayList<LatLng> poly = new ArrayList<LatLng>();
	        int index = 0, len = encoded.length();
	        int lat = 0, lng = 0;
	        while (index < len) {
	            int b, shift = 0, result = 0;
	            do {
	                b = encoded.charAt(index++) - 63;
	                result |= (b & 0x1f) << shift;
	                shift += 5;
	            } while (b >= 0x20);
	            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	            lat += dlat;
	            shift = 0;
	            result = 0;
	            do {
	                b = encoded.charAt(index++) - 63;
	                result |= (b & 0x1f) << shift;
	                shift += 5;
	            } while (b >= 0x20);
	            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	            lng += dlng;

	            LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
	            poly.add(position);
	        }
	        return poly;
	    }
	}
	
	private String TAG = "STLocationManager";
	// Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
    
    LocationRequest mLocationRequest;
    
    public interface STLocationManagerCallback {
		void onResult(Location location, String errorMessage);
	}
	
	
	private LocationClient mLocationClient;
	private static STLocationManager mInstance;
	private Location mCurrentLocation;
	private Context mContext;
	
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	private ConnectionResult connectionResult;

	public STLocationManager() {
	
	}
	
	public static STLocationManager getInstance () {
		if(mInstance == null) {
			mInstance = new STLocationManager();
			mInstance.mLocationRequest = LocationRequest.create();
			
			//Use high accuracy
			mInstance.mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
			
			//set the update interval to 5 secs
			mInstance.mLocationRequest.setInterval(UPDATE_INTERVAL);
			
			//set the fastest update interval
			mInstance.mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
		}
		
		return mInstance;
	}
	
	private void buildAlertMessageNoGps(final Context ctx) {
	    final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
	    builder.setTitle("GPS is disabled.")
	    	   .setMessage("SecureTrike requires you to enable it. Selecting 'No' will terminate this app.")
	           .setCancelable(false)
	           .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	                   ctx.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	               }
	           })
	           .setNegativeButton("No", new DialogInterface.OnClickListener() {
	               public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	                    dialog.cancel();
	                    ((Activity) ctx).finish();
	               }
	           });
	    final AlertDialog alert = builder.create();
	    alert.show();
	}
	
	public boolean checkIfGPSIsEnabled (Context ctx) {
		final LocationManager manager = (LocationManager) ctx.getSystemService( Context.LOCATION_SERVICE );

	    if (!manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
	        buildAlertMessageNoGps(ctx);
	        return false;
	    }
	    
	    return true;
	}
	
	public void getLastLocation(final STLocationManagerCallback callback) {
		
		AndroidHttpClient httpClient = new AndroidHttpClient("http://maps.google.com");
		 httpClient.setMaxRetries(5);
	     httpClient.addHeader("Authorization", "Basic YmFibHJAYmFibHIuY29tOnBhc3N3b3Jk");
	        
	     String pos = "" + mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude();
	     httpClient.get("/maps/api/directions/json?origin="+pos+"&destination="+pos+"&sensor=true", null, new AsyncCallback() {

			@Override
			public void onComplete(com.turbomanage.httpclient.HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				Log.d(TAG, httpResponse.getBodyAsString() + " " + httpResponse.getUrl());
				JSONObject object  = null;
				try {
					object = new JSONObject(httpResponse.getBodyAsString());

					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
				
				if(object != null) {
					JSONArray routes = null;
					try {
						routes = object.getJSONArray("routes");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JSONObject route = null;
					try {
						route = routes.getJSONObject(0);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					JSONObject bounds = null;
					try {
						bounds = route.getJSONObject("bounds");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(bounds != null) {
						JSONObject northeast = null;
						try {
							northeast = bounds.getJSONObject("northeast");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(northeast != null) {
							try {
								mCurrentLocation.setLatitude(northeast.getDouble("lat"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							try {
								mCurrentLocation.setLongitude(northeast.getDouble("lng"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							callback.onResult(mCurrentLocation, null);
						}else {
							callback.onResult(mCurrentLocation, "No valid JSON object " + httpResponse.getUrl());
						}
					}else {
						callback.onResult(mCurrentLocation, "No valid JSON object " + httpResponse.getUrl());
					}
					
				}else {
					callback.onResult(mCurrentLocation, "No valid JSON object " + httpResponse.getUrl());
				}
			}	
			
			public void onError(Exception e) {
		        e.printStackTrace();
		        callback.onResult(mCurrentLocation, e.getMessage());
		    }

	     });
	}
	

	
	public void startService (Context context) {
		if(this.mLocationClient == null)
			this.mLocationClient = new LocationClient(context, this, this);
		
		this.mLocationClient.connect();
	}
	
	public void stopService () {
		if(this.mLocationClient != null && this.mLocationClient.isConnected()) {
			this.mLocationClient.removeLocationUpdates(this);
		}
		
		this.mLocationClient.disconnect();
		this.mLocationClient = null;
	
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		/*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                		(Activity) mContext,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            //showErrorDialog(connectionResult.getErrorCode());
        }
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		this.mCurrentLocation = this.mLocationClient.getLastLocation();
		
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}

	@Override
	public void onDisconnected() {
		
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.d(TAG,"Location: " + location.getProvider() + " at " + location.getLatitude() + ", " + location.getLongitude());
		mCurrentLocation = location;
		//Toast.makeText(this.mContext, "Location: " + location.getProvider() + " at " + location.getLatitude() + ", " + location.getLongitude(), Toast.LENGTH_SHORT).show();
	}
}

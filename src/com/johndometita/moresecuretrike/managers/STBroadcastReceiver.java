package com.johndometita.moresecuretrike.managers;

import com.johndometita.moresecuretrike.STMainFragmentActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class STBroadcastReceiver extends BroadcastReceiver {

	public STBroadcastReceiver() {
		
	}

	@Override
	public void onReceive(Context ctx, Intent arg1) {
		Intent main = new Intent(ctx, STMainFragmentActivity.class);
		main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(main);
	}

}
